import React from 'react';
import { View, Text, Button } from 'react-native';

function LoginScreen({ navigation }) {
  return (
    <View>
      <Text>Màn hình Đăng nhập</Text>
      <Button title="Vào home" onPress={() => navigation.navigate('Home')} />
    </View>
  );
}

export default LoginScreen;