import React from 'react';
import { View, Text, Button } from 'react-native';

function HomeScreen({ navigation }) {
  return (
    <View>
      <Text>Màn hình Home</Text>
      <Button title="Sign Out" onPress={() => navigation.navigate('Login')} />
    </View>
  );
}

export default HomeScreen;
